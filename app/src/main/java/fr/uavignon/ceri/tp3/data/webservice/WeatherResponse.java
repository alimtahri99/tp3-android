package fr.uavignon.ceri.tp3.data.webservice;

import java.util.List;

public class WeatherResponse {

    public final List <Weather> weather=null;
    public final Main main=null;
    public final Long dt=null;
    public final Wind wind=null;
    public final Clouds clouds=null;



    public static class Main{
        public final float temp=0;
        public final Integer humidity=0;

    }


    public static class Clouds{

        public final int all=0;

    }

    public static class  Weather{
        public String description;
        public  String icon;

    }

    public static  class Wind{
        public float speed;
        public float deg;

    }





}
