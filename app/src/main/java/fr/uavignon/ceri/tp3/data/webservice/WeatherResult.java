package fr.uavignon.ceri.tp3.data.webservice;


import android.util.Log;

import fr.uavignon.ceri.tp3.data.City;

public class WeatherResult {

    static void transferInfo(WeatherResponse weatherInfo, City cityInfo){


        cityInfo.setDescription(weatherInfo.weather.get(0).description);
        cityInfo.setIcon(weatherInfo.weather.get(0).icon);
        cityInfo.setHumidity(weatherInfo.main.humidity);
        cityInfo.setWindSpeedMPerS(weatherInfo.wind.speed);
        cityInfo.setTemperature(weatherInfo.main.temp);
        cityInfo.setCloudiness(weatherInfo.clouds.all);
        cityInfo.setLastUpdate(weatherInfo.dt);





    }


}
